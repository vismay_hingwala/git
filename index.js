const newman = require('newman');
const fs = require('fs');

newman.run({
    collection: require('./MPDC.postman_collection.json'),
    reporters: 'cli'
}).on('beforeRequest', function (error, args) {
    if (error) {
        console.error(error);
    } else {
        fs.writeFile('./request.txt', args.request.body.raw, function (error) {
            if (error) { 
                console.error(error); 
            }
        });    
    }
}).on('request', function (error, args) {
    if (error) {
        console.error(error);
    }
    else {
        fs.writeFile('./response.txt', args.response.stream, function (error) {
            if (error) { 
                console.error(error); 
            }
        });        
    }
});